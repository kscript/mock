interface anyObject {
    [prop: string]: any
}
interface stringObject {
    [prop: string]: string
}
interface functionObject {
    [prop: string]: Function
}
